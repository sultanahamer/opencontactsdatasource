package open.com.opencontactsdatasource.utils;

import android.os.Handler;
import android.os.Looper;

public class Thread {
    public static void processAsync(final Runnable someRunnable) {
        new java.lang.Thread() {
            @Override
            public void run() {
                someRunnable.run();
            }
        }.start();
    }


    public static void runOnMainDelayed(final Runnable someRunnable, long delayInMillis) {
        new Handler(Looper.getMainLooper()).postDelayed(someRunnable, delayInMillis);
    }

}
