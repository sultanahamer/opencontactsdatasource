package open.com.opencontactsdatasource.utils.functional;

import static open.com.opencontactsdatasource.utils.Thread.runOnMainDelayed;

import open.com.opencontactsdatasource.utils.functional.io.IO;

public class Android {
    public static <Y> IO<Y, Y> presentOnMainIO(Class<Y> typeToCreateResultantIO) {
        return new IO<Y,Y>((value, onSuccess, onFailure) -> {
            runOnMainDelayed(() -> onSuccess.accept(value), 0);
        });
    }
}
