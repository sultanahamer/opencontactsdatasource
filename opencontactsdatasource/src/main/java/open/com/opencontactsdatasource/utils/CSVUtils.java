package open.com.opencontactsdatasource.utils;

import static java.util.Arrays.asList;

import androidx.core.util.Pair;

import com.github.underscore.U;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;

public class CSVUtils {
    public static List<Pair<String, List<String>>> parseNameAndPhoneNumbers(String csvData) throws IOException, CsvException {
        List<String[]> rows = readAllRows(csvData);
        return U.map(rows, row -> new Pair<>(row[0], U.rest(asList(row))));
    }
    public static List<String[]> readAllRows(String csvData) throws IOException, CsvException {
        return csvReader(csvData).readAll();
    }

    public static CSVReader csvReader(String csvData) {
        return new CSVReaderBuilder(new StringReader(csvData))
                .build();
    }

}
