package open.com.opencontactsdatasource.utils.functional;

public interface Consumer<X> {
    void accept(X value);
}
