package open.com.opencontactsdatasource.utils.functional;


//public interface ProcessThatCanThrow<X,Y, F extends FunctionThatCanThrow<X, Y>> {
//    void process(F function, Consumer<Y> onSuccess, Consumer<Exception> onFailure);
//}

public interface ProcessThatCanThrow<X, Y> {
    void process(X value, Consumer<Y> onSuccess, Consumer<Exception> onFailure);
}
