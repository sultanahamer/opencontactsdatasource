package open.com.opencontactsdatasource.utils;

import static open.com.opencontactsdatasource.utils.Thread.processAsync;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;


import open.com.opencontactsdatasource.utils.functional.Consumer;
import open.com.opencontactsdatasource.utils.functional.Nothing;
import open.com.opencontactsdatasource.utils.functional.io.IO;
import open.com.opencontactsdatasourcecontract.ContactsDataStoreService;

public class RunOnService {
    public static IO<Nothing, ContactsDataStoreService> withService(Context context) {
        return new IO<>((value, onSuccess, onFailure) -> {
            processAsync( () -> callWithService(onSuccess, onFailure, context));
        });
    }
    
    private static void callWithService(Consumer<ContactsDataStoreService> runOnService, Consumer<Exception> onFailure, Context context) {
        ServiceConnection serviceConnection = new ServiceConnection() {
            @Override
            public void onBindingDied(ComponentName name) {
                System.out.println("binding died yolo");
            }

            @Override
            public void onNullBinding(ComponentName name) {
                System.out.println("null binding yolo");
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                System.out.println("yolo connected");
                ContactsDataStoreService contactsDataStoreService = ContactsDataStoreService.Stub.asInterface(service);
                runOnService.accept(contactsDataStoreService);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                System.out.println("Disconnected... yolo");
            }
        };
        Intent intent = new Intent("DataSourceService");
        //TODO: Find better way of finding the class to target below, this is quite ugly
        //System.out.println(ContactsDataStoreService.class.getPackage().getName() + ".debug, yolo");
//        intent.setPackage(ContactsDataStoreService.class.getPackage().getName() + ".debug");
        //intent.setPackage(ContactsDataStoreService.class.getPackage().getName());
        intent.setPackage("opencontacts.open.com.opencontacts.debug");
        boolean connected = context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        System.out.println("yolo connected: " + connected);
        if(!connected) onFailure.accept(new Exception("Failed connecting to service"));
    }
}
