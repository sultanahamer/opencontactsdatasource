package open.com.opencontactsdatasource;

import androidx.core.util.Pair;

import java.util.List;

import open.com.opencontactsdatasource.utils.CSVUtils;
import open.com.opencontactsdatasourcecontract.ContractMethods;

public class ContractMethodConsumers {
    public static ContractMethodConsumer<List<Pair<String, List<String>>> > fetchAllNamesAndPhoneNumbersV1 = new ContractMethodConsumer<>(
                    ContractMethods.fetchAllNamesAndPhoneNumbersV1,
                    CSVUtils::parseNameAndPhoneNumbers
    );

    public static ContractMethodConsumer<List<Pair<String, List<String>>> > fetchNameForPhoneNumberV1 = new ContractMethodConsumer<>(
            ContractMethods.fetchNamesForPhoneNumbersV1,
            CSVUtils::parseNameAndPhoneNumbers
    );
}
