package open.com.opencontactsdatasource;

import static java.util.Arrays.asList;
import static open.com.opencontactsdatasource.Storage.getAuthCode;
import static open.com.opencontactsdatasource.Storage.permissionsGranted;
import static open.com.opencontactsdatasource.utils.RunOnService.withService;
import static open.com.opencontactsdatasourcecontract.Contract.PermissionsActivity.PERMISSIONS_EXTRA;
import static open.com.opencontactsdatasourcecontract.Contract.PermissionsActivity.RESULT_AUTH_CODE;
import static open.com.opencontactsdatasourcecontract.Contract.getErrorMessageV1;
import static open.com.opencontactsdatasourcecontract.Contract.isErrorResponseV1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.github.underscore.U;
import com.opencsv.CSVWriter;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import open.com.opencontactsdatasource.utils.functional.Consumer;
import open.com.opencontactsdatasource.utils.functional.FunctionThatCanThrow;
import open.com.opencontactsdatasource.utils.functional.Nothing;
import open.com.opencontactsdatasource.utils.functional.io.IO;
import open.com.opencontactsdatasourcecontract.Contract;
import open.com.opencontactsdatasourcecontract.ContractMethod;

public class ContractMethodConsumer<Y> {
    public final ContractMethod contractMethod;
    private final FunctionThatCanThrow<String, Y> postProcess;

    //    LOOKS better than startActivityForResult as there is no dependency on activity's onActivityResult call back
    public static Intent getIntentToRequestPermissions(List<ContractMethodConsumer> contractMethodConsumers) {
        List<String> permissionsRequired = permissionsRequiredToCall(contractMethodConsumers);

        return new Intent(Contract.PermissionsActivity.LAUNCH_ACTION)
                .putExtra(PERMISSIONS_EXTRA, permissionsRequired.toArray(new String[]{}));

    }

    private static List<String> permissionsRequiredToCall(List<ContractMethodConsumer> contractMethodConsumers) {
        List<String> permissionsRequired = U.chain(contractMethodConsumers)
                .map(consumer -> consumer.contractMethod.permissionsRequired)
                .flatten()
                .uniq()
                .value();
        return permissionsRequired;
    }

    public static <Y> void permissionsResultHandler(Context context, int resultCode, Intent resultData, Runnable onSuccess, Consumer<Exception> onFailure) {
            if (resultCode != Activity.RESULT_OK) {
                onFailure.accept(new Exception("User didn't authorize"));
                return;
            }
            if (resultData == null) {
                onFailure.accept(new Exception("Couldn't get auth code"));
                return;
            }
            String authCode = resultData.getStringExtra(RESULT_AUTH_CODE);
            if (authCode == null) {
                onFailure.accept(new Exception("Couldn't get auth code"));
                return;
            }

            Storage.writeAuthCode(authCode, context);
            Storage.updatePermissionsGranted(context, asList(resultData.getStringArrayExtra(PERMISSIONS_EXTRA)));
            onSuccess.run();
    }

    private String throwIfError(String response) throws Exception {
        if (isErrorResponseV1(response)) throw new Exception(getErrorMessageV1(response));
        return response;
    }

    private String argsWithMethodDetails(String[] args) throws IOException {
        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = new CSVWriter(writer);
        csvWriter.writeNext(U.concat(new String[]{contractMethod.name, contractMethod.version}, args));
        csvWriter.flush();
        return writer.getBuffer().toString();
    }

    private IO<Nothing, String> callContractMethod(Context context, String... args) {
        return withService(context)
                .map(service -> service.call(getAuthCode(context), argsWithMethodDetails(args)))
                .map(this::throwIfError);
    }

    public IO<Nothing, Y> process(Context context, String... args) {
        return callContractMethod(context, args)
                .map(postProcess);
    }

    public ContractMethodConsumer(ContractMethod contractMethod, FunctionThatCanThrow<String, Y> postProcess) {
        this.contractMethod = contractMethod;
        this.postProcess = postProcess;
    }

    public static boolean hasPermissionsToCall(List<ContractMethodConsumer> consumers, Context context) {
        return permissionsGranted(context).containsAll(permissionsRequiredToCall(consumers));
    }
}
