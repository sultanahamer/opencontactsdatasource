package open.com.opencontactsdatasource;

import static java.util.Collections.emptySet;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.Nullable;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

class Storage {
    private static final String PREFERNCES_FILE_NAME = "opencontacts_data_store";
    private static final String AUTHCODE_PREF_KEY = "auth_code";
    private static final String PERMISSIONS_GRANTED_PREF_KEY = "permissions";

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREFERNCES_FILE_NAME,  Context.MODE_PRIVATE);
    }

    @Nullable
    static String getAuthCode(Context context) {
        return getSharedPreferences(context).getString(AUTHCODE_PREF_KEY, null);
    }

    static Set<String> permissionsGranted(Context context) {
        return getSharedPreferences(context).getStringSet(PERMISSIONS_GRANTED_PREF_KEY, emptySet());
    }

    static void updatePermissionsGranted(Context context, List<String> grantedPermissions) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        HashSet<String> permissions = new HashSet<>(sharedPreferences.getStringSet(PERMISSIONS_GRANTED_PREF_KEY, emptySet()));
        permissions.addAll(grantedPermissions);
        sharedPreferences.edit().putStringSet(PERMISSIONS_GRANTED_PREF_KEY, permissions).apply();
    }

    static void writeAuthCode(String authCode, Context context) {
        getSharedPreferences(context).edit()
                .putString(AUTHCODE_PREF_KEY, authCode)
                .apply();
    }
}
